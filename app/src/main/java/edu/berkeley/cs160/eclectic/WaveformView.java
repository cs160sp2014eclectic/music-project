package edu.berkeley.cs160.eclectic;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Shahin on 4/18/2014.
 */
public class WaveformView extends View {

    Object lock = new Object();
    int[] audioBuffer;
    Paint orangePaint;
    Paint bluePaint;
    Paint backOrange;
    Paint backBlue;
    Paint inUsePaint;
    int w, h;
    boolean firstTime = true;
    double ratio;
    long time;

    public WaveformView(Context context,  AttributeSet attrs){
        super(context, attrs);
        orangePaint = setPaint(1, Color.parseColor("#FFD8AB"));
        backOrange = setPaint(1,R.color.backgroundOrange);
        bluePaint = setPaint(1, Color.parseColor("#BCE7F6"));
        backBlue = setPaint(1,R.color.backgroundBlue);
        inUsePaint = bluePaint;
        time = System.currentTimeMillis();
    }


    public void toggleNormalPaint() {
        inUsePaint = bluePaint;
    }

    public void toggleErrorPaint() {
        inUsePaint = orangePaint;
    }

    @Override
    public void onDraw(Canvas canvas){
        super.onDraw(canvas);
        if (firstTime){
            w = getWidth();
            h = getHeight();
            firstTime = false;
            ratio = (double)(BeatProcessor.soundBufferAmplitude.length)/(double)w;
        }
        if (System.currentTimeMillis() - time > 30) {
            time = System.currentTimeMillis();
            for (int i = 0; i < w; i++) {
                canvas.drawLine(i, h / 2, i, h / 2 + (int) (BeatProcessor.soundBufferAmplitude[(int)
                                (i * ratio)] * 3 / 256),
                        inUsePaint
                );
                canvas.drawLine(i, h / 2, i, h / 2 - (int) (BeatProcessor.soundBufferAmplitude[(int)
                                (i * ratio)] * 3 / 256),
                        inUsePaint
                );
            }
            invalidate();
        }
    }

    public Paint setPaint(float width, int color){
        Paint p = new Paint();
        p.setDither(true);
        p.setStyle(Paint.Style.STROKE);
        p.setStrokeJoin(Paint.Join.ROUND);
        p.setStrokeCap(Paint.Cap.ROUND);
        p.setStrokeWidth(width);
        p.setColor(color);
        return p;
    }

}
