package edu.berkeley.cs160.eclectic;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class StatActivity extends Activity {

    public static final String SONG_ID="com.cs160.eclectic.ID";

    int songID;
    Song song;
    TextView song_title;
    TextView song_author;
    TextView song_key;
    TextView song_mode;
    TextView song_goal_desired;
    TextView song_goal_achieved;
    ListView practice_feed;

    WebView visualization;
    int offset=0;

    ArrayList<Practice> practices;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stat);

        Bundle extras = getIntent().getExtras();
        songID = extras.getInt(SONG_ID);
        song = JournalActivity.db.getSong(songID);

        song_title = (TextView) findViewById(R.id.song_title_text);
        song_title.setText(Utils.capitalizeFirstChar(song.title));

        song_author = (TextView) findViewById(R.id.author_text);
        song_author.setText(Utils.capitalizeFirstChar(song.author));

        song_key = (TextView) findViewById(R.id.song_key_text);
        song_key.setText(song.key);

        song_mode = (TextView) findViewById(R.id.song_mode_text);
        song_mode.setText(song.mode);

        practice_feed = (ListView) findViewById(R.id.practice_feed_list);

        TextView textView = new TextView(this);
        textView.setText("PRACTICE FEED");
        textView.setPadding(20, 50, 20, 50);
        practice_feed.addHeaderView(textView);
        updatePractices();

        song_goal_achieved = (TextView) findViewById(R.id.goal_matched_text);
        song_goal_achieved.setText(Utils.capitalizeFirstChar(createGoalMatchedString(practices)));

        song_goal_desired = (TextView) findViewById(R.id.desired_goal_text);
        song_goal_desired.setText(createGoalDesiredString());

        visualization = (WebView) findViewById(R.id.visualization);
        visualization.getSettings().setJavaScriptEnabled(true);
        visualization.addJavascriptInterface(this, "webConnector");
        visualization.setWebViewClient(new VisualizationWebViewClient());

        visualization.setOnTouchListener(new OnSwipeTouchListener(this){
            @Override
            public void onSwipeLeft() {
                offset -= 3;
                updateWebView();

            }

            public void onSwipeRight(){
                offset += 3;
                updateWebView();
            }
        });


    }

    private class VisualizationWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url)
        {
            view.loadUrl(url);
            return true;
        }
    }

    @JavascriptInterface
    public String load() {
        return createSimpleJSON(practices);
    }

    @JavascriptInterface
    public int getOffsetFromAndroid() {
        return offset;
    }

    private void updateWebView(){
        visualization.loadUrl("file:///android_asset/index.html");
    }

    private long convertToEpoch(String date){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
        try {
            Date d = df.parse(date);
            long epoch = d.getTime();
            return epoch;
        }catch (ParseException e) {
            return -1;
        }
    }

    private String OnlyDate(Practice p){
        String date = p.date;
        return date.split("\\s+")[0];
    }

    private HashMap<String, Integer> createDateTimePair(ArrayList<Practice> practices){
        HashMap<String, Integer> result = new HashMap<String, Integer>();
        for (int i=0; i<practices.size(); i++){
            String date = OnlyDate(practices.get(i));
            if(result.containsKey(date)){
                result.put(date, result.get(date) + practices.get(i).ptime);
            }else{
                result.put(date, practices.get(i).ptime);
            }
        }
        return result;
    }

    private String createSimpleJSON(ArrayList<Practice> practices){
        HashMap<String, Integer> dateTimePair = createDateTimePair(practices);
        StringBuilder JSON = new StringBuilder();
        if (!dateTimePair.isEmpty()) {
            Iterator it = dateTimePair.entrySet().iterator();
            JSON.append("{ ");
            while(it.hasNext()) {
                Map.Entry<String, Integer> next = (Map.Entry<String, Integer>) it.next();
                JSON = JSON.append("\"" + next.getKey() + "\": ");

                JSON = JSON.append("\"" + (double)next.getValue()/60 + "\" ");

                if (it.hasNext()) {
                    JSON.append(", ");
                } else {
                    JSON.append(" }");
                }
            }
        }
        return JSON.toString();
    }

    private ArrayList<HashMap<String, String>> createPracticeHashmaps(ArrayList<Practice> practices){
        ArrayList<HashMap<String, String>> result = new ArrayList<HashMap<String, String>>();
        if(!practices.isEmpty()){
            for(Practice p: practices){
                HashMap<String, String> data = new HashMap<String, String>();
                data.put("date", "Last practiced " + Utils.createPrettyRelativeDate(p.date) + " " +
                        "for " + p.ptime
                        + " " +
                        "minutes");
                //data.put("comments", "Last Practiced on "+ c.ctime);
                result.add(data);
            }
        }
        return result;
    }

    private String createGoalMatchedString(ArrayList<Practice> practices) {
        return calculateAchievedPractice() + " of practice goal met";
    }

    private String createGoalDesiredString() {
        return "Goal of " + song.goal + " hours per week";
    }

    protected void onResume(){
        super.onResume();
        updatePractices();
        updateWebView();
    }

    private void updatePractices(){
        practices = JournalActivity.db.getAllPractices(songID);
        Collections.reverse(practices);
        SimpleAdapter adapter = new SimpleAdapter(this, createPracticeHashmaps(practices),
                R.layout.practice_entry_list_item,
                new String[] {"date"},
                new int[] {android.R.id.text1,
                        android.R.id.text2});
        practice_feed.setAdapter(adapter);

        practice_feed.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    // do nothing -- this is the header title
                    return;
                }
                Practice selected = practices.get(i-1);
                Intent practiceEntry = new Intent(StatActivity.this, PracticeEntryActivity.class);
                practiceEntry.putExtra("entry", selected);
                startActivity(practiceEntry);
            }
        });
    }


    private String calculateAchievedPractice() {
        practices = JournalActivity.db.getAllPractices(songID);
        int totalMinutes = 0;
        //TODO: need to actually account for the past week instead of all practices
        for (Practice p: practices) {
            totalMinutes += p.ptime;
        }
        double numHours = (new Double(totalMinutes)) / 60.0;
        double goal = numHours / (new Double(song.goal));
        if (goal >= 1.0) {
            return "100%";
        }
        return "" + ((int) (goal * 100)) + "%";
    }



}
