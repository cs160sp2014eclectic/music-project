package edu.berkeley.cs160.eclectic;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


/**
 * Created by Shahin on 4/5/2014.
 */
public class DataBaseHandler extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "myDataBase.db";

    private static final String SONG_TABLE_NAME = "SongEntries";
    private static final String PRACTICE_TABLE_NAME = "PracticeEntries";
    private static final String COMMENT_TABLE_NAME = "CommentEntries";

    public static final String SONG_ID = "sid";
    public static final String TITLE = "title";
    public static final String AUTHOR = "author";
    public static final String KEY  = "key";
    public static final String MODE  = "mode";
    public static final String GOAL  = "goal";
    public static final String STIME  = "time";

    public static final String PRACTICE_ID = "pid";
    public static final String SID = "sid";
    public static final String SCORE = "score";
    public static final String DATE = "date";
    public static final String TEMPO = "tempo";
    public static final String PTIME  = "time";

    public static final String COMMENT_ID = "cid";
    public static final String COMMENT = "comment";
    public static final String PID = "pid";
    public static final String CTIME  = "time";

    public static final String createEntryDB = "create table "+SONG_TABLE_NAME+ " ("
            +SONG_ID+" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
            +TITLE+" text, "
            +AUTHOR+" text, "
            +KEY+" text, "
            +MODE+" text, "
            +GOAL+" integer, "
            +STIME+" text);";

    public static final String createPracticeDB = "create table "+PRACTICE_TABLE_NAME+ " ("
            +PRACTICE_ID+" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
            +SCORE+" text, "
            +DATE+" text, "
            +TEMPO+" integer, "
            +PTIME+" integer, "
            +SID+" integer, "
            +"FOREIGN KEY("+SID+") REFERENCES "+SONG_TABLE_NAME+"("+SONG_ID+")"
            +");";


    public static final String createCommentDB = "create table "+COMMENT_TABLE_NAME+ " ("
            +COMMENT_ID+" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
            +COMMENT+" text, "
            +CTIME+" text, "
            +PID+" integer, "
            +"FOREIGN KEY("+PID+") REFERENCES "+PRACTICE_TABLE_NAME+"("+PRACTICE_ID+")"
            +");";

    public DataBaseHandler(Context context, CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, version);
    }

    public long insertSong  (Song song)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        //contentValues.put(SONG_ID, song.sid);
        contentValues.put(TITLE, song.title);
        contentValues.put(AUTHOR, song.author);
        contentValues.put(KEY, song.key);
        contentValues.put(MODE, song.mode);
        contentValues.put(GOAL, song.goal);
        contentValues.put(STIME, this.getDateTime());

        return db.insert(SONG_TABLE_NAME, null, contentValues);
    }

    public Song getSong(int sid) {
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT  * FROM " + SONG_TABLE_NAME + " WHERE "
                + SONG_ID + " = " + sid;

        Cursor c = db.rawQuery(query, null);

        if (c != null) {
            c.moveToFirst();
        }

        int id = c.getInt(c.getColumnIndex(SONG_ID));
        String title = c.getString(c.getColumnIndex(TITLE));
        String author = c.getString(c.getColumnIndex(AUTHOR));
        String key = c.getString(c.getColumnIndex(KEY));
        String mode = c.getString(c.getColumnIndex(MODE));
        int goal = c.getInt(c.getColumnIndex(GOAL));
        String time = c.getString(c.getColumnIndex(STIME));


        Song song = new Song(id, title, author, key, mode, goal, time);

        return song;
    }

    public ArrayList<Song> getAllSongs() {
        ArrayList<Song> allSongs = new ArrayList<Song>();
        String query = "SELECT  * FROM " + SONG_TABLE_NAME;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();
        while (!c.isAfterLast()){
            int id = c.getInt(c.getColumnIndex(SONG_ID));
            String title = c.getString(c.getColumnIndex(TITLE));
            String author = c.getString(c.getColumnIndex(AUTHOR));
            String key = c.getString(c.getColumnIndex(KEY));
            String mode = c.getString(c.getColumnIndex(MODE));
            int goal = c.getInt(c.getColumnIndex(GOAL));
            String time = c.getString(c.getColumnIndex(STIME));

            Song song = new Song(id, title, author, key, mode, goal, time);
            allSongs.add(song);

            c.moveToNext();
        }


        return allSongs;
    }

    public int updateSong(int sid, String title, String author, String key, String mode, int goal) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TITLE, title);
        values.put(AUTHOR, author);
        values.put(KEY, key);
        values.put(MODE, mode);
        values.put(GOAL, goal);

        // updating row
        return db.update(SONG_TABLE_NAME, values, SID+" = ?", new String[] {sid+""});
    }

    public boolean deleteSong(int sid){
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(SONG_TABLE_NAME, SONG_ID + "=" + sid, null) > 0;
    }

    public boolean deletePracticeComments(int sid){
        boolean successful=true;
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<Practice> practices = getAllPractices(sid);
        for (Practice p: practices){
            successful = successful && (db.delete(PRACTICE_TABLE_NAME, PRACTICE_ID+"="+p.pid, null)>0) &&
                    (db.delete(COMMENT_TABLE_NAME, PID+"="+p.pid, null)>0);
        }
        return successful;
    }

    public long insertPractice  (Practice practice)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        //contentValues.put(PRACTICE_ID, practice.pid);
        contentValues.put(SID, practice.sid);
        contentValues.put(SCORE, practice.score);
        contentValues.put(DATE, practice.date);
        contentValues.put(TEMPO, practice.tempo);
        contentValues.put(PTIME, practice.ptime);

        // will return the row number of the practice session
        return db.insert(PRACTICE_TABLE_NAME, null, contentValues);
    }

    public Practice getPractice(int sid, int pid) {
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT  * FROM " + PRACTICE_TABLE_NAME + " WHERE "
                + SONG_ID + " = " + sid
                + " AND "
                + PRACTICE_ID+ " = "+ pid;

        Cursor c = db.rawQuery(query, null);

        if (c != null) {
            c.moveToFirst();
        }

        int id = c.getInt(c.getColumnIndex(PRACTICE_ID));
        int sID = c.getInt(c.getColumnIndex(SID));
        String score = c.getString(c.getColumnIndex(SCORE));
        String date = c.getString(c.getColumnIndex(DATE));
        int tempo = c.getInt(c.getColumnIndex(TEMPO));
        int ptime = c.getInt(c.getColumnIndex(PTIME)); // ptime is how long the user has
        // practiced.


        Practice practice = new Practice(id, sID, score, date, tempo, ptime);

        return practice;
    }

    public int getPracticeIdFromRowId(long rid) {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT "+ PRACTICE_ID +" FROM " + PRACTICE_TABLE_NAME
                + " WHERE ROWID = " + rid;
        Cursor c = db.rawQuery(query, null);
        if (c != null) {
            c.moveToFirst();
        }
        return c.getInt(c.getColumnIndex(PRACTICE_ID));
    }

    public int getSongIdFromRowId(long rid) {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT "+ SONG_ID +" FROM " + SONG_TABLE_NAME
                + " WHERE ROWID = " + rid;
        Cursor c = db.rawQuery(query, null);
        if (c != null) {
            c.moveToFirst();
        }
        return c.getInt(c.getColumnIndex(SONG_ID));
    }

    public ArrayList<Practice> getAllPractices(int sid) {
        ArrayList<Practice> allPractices = new ArrayList<Practice>();
        String query = "SELECT  * FROM " + PRACTICE_TABLE_NAME+ " WHERE "
                + SID + " = " + sid;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();
        while (!c.isAfterLast()){
            int id = c.getInt(c.getColumnIndex(PRACTICE_ID));
            int sID = c.getInt(c.getColumnIndex(SID));
            String score = c.getString(c.getColumnIndex(SCORE));
            String date = c.getString(c.getColumnIndex(DATE));
            int tempo = c.getInt(c.getColumnIndex(TEMPO));
            int ptime = c.getInt(c.getColumnIndex(PTIME));


            Practice practice = new Practice(id, sID, score, date, tempo, ptime);
            allPractices.add(practice);

            c.moveToNext();
        }
        return allPractices;
    }

    public int updatePractice(int pid, int sid, int t, String score) {
        SQLiteDatabase db = this.getWritableDatabase();
        Practice p = this.getPractice(sid, pid);
        ContentValues values = new ContentValues();
        values.put(SCORE, score);
        values.put(DATE, p.date);
        values.put(TEMPO, p.tempo);
        values.put(PTIME, t);

        // updating row
        return db.update(PRACTICE_TABLE_NAME, values, PRACTICE_ID +" = ? AND "+SID+" = ?", new String[] {pid+"",sid+""});
    }

    public int deletePractice(int pid, int sid){
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(PRACTICE_TABLE_NAME, PRACTICE_ID +" = ? AND "+SID+ " = ?", new String[] {pid+"", sid+""});
    }

    public boolean insertComment(Comment comment)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        //contentValues.put(COMMENT_ID, comment.cid);
        contentValues.put(COMMENT, comment.comment);
        contentValues.put(CTIME, getDateTime());
        contentValues.put(PID, comment.pid);


        db.insert(COMMENT_TABLE_NAME, null, contentValues);
        return true;
    }

    public ArrayList<Comment> getAllComments(int pid) {

        ArrayList<Comment> allComments = new ArrayList<Comment>();
        String query = "SELECT  * FROM " + COMMENT_TABLE_NAME+ " WHERE "
                + PID + " = " + pid;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();
        while (!c.isAfterLast()){
            int commentId = c.getInt(c.getColumnIndex(COMMENT_ID));
            String text = c.getString(c.getColumnIndex(COMMENT));
            String time = c.getString(c.getColumnIndex(CTIME));
            int practiceId = c.getInt(c.getColumnIndex(PID));


            Comment comment = new Comment(commentId, text, practiceId, time);
            allComments.add(comment);

            c.moveToNext();
        }
        return allComments;
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(createEntryDB);
        db.execSQL(createPracticeDB);
        db.execSQL(createCommentDB);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int old_version, int new_version) {
        db.execSQL("DROP TABLE IF EXISTS "+SONG_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+PRACTICE_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+COMMENT_TABLE_NAME);

        onCreate(db);
    }

}
