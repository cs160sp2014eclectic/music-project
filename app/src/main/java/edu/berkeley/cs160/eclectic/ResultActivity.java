package edu.berkeley.cs160.eclectic;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ResultActivity extends Activity {

    public static final String SONG_ID="com.cs160.eclectic.ID";
    public static final String Practice_ID="com.cs160.eclectic.Practice_ID";
    public static final String SAVE_Comment_ID="com.cs160.eclectic.SAVECOMMENTID";
    static int commentID = 1;

    int songID;
    int practiceID;

    EditText comment;
    AlertDialog.Builder alertDialogBuilder;
    AlertDialog alertDialog;

    LinearLayout goToDashboard;
    TextView theScore;
    TextView thePracticeTime;

    Intent dashboard;
    TextView notesHeader;
    LinearLayout notesLayout;
    Context that;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        if(savedInstanceState != null){
            songID = savedInstanceState.getInt(SAVE_Comment_ID);
        }
        that = this;

        Bundle extras = getIntent().getExtras();
        songID = extras.getInt(SONG_ID);
        practiceID = extras.getInt(Practice_ID);

        Song song = JournalActivity.db.getSong(songID);
        Practice practice = JournalActivity.db.getPractice(songID, practiceID);


        theScore = (TextView) findViewById(R.id.practiceScore);
        thePracticeTime = (TextView) findViewById(R.id.practiceTime);
        theScore.setText("Score: "+practice.score);
        thePracticeTime.setText("You practiced for "+ formatTime(practice.ptime));

        TextView tempoText = (TextView) findViewById(R.id.tempoText);
        tempoText.setText("Tempo set to " + practice.tempo + " BPM");

        TextView songTitle = (TextView) findViewById(R.id.pEntryTitle);
        songTitle.setText(Utils.capitalizeFirstChar(song.title));

        notesHeader = (TextView) findViewById(R.id.notesHeader);
        notesLayout = (LinearLayout) findViewById(R.id.pEntryNotesLayout);


        setTitle(song.title);

        comment = new EditText(this);
        LinearLayout.LayoutParams editTextPrameter = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        comment.setLayoutParams(editTextPrameter);

        alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(R.string.note);
        alertDialogBuilder.setView(comment);
        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                comment.setText("");
                // Do nothing
                dialog.cancel();
            }
        });
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Comment c = new Comment(commentID, comment.getText().toString(), practiceID, "");
                JournalActivity.db.insertComment(c);
                comment.setText("");
                commentID++;

                // UI update stuff
                notesHeader.setText("Your notes about this practice session:");
                TextView tv = new TextView(that);
                tv.setText("- " + c.comment);
                tv.setPadding(0, 10, 0, 10);
                notesLayout.addView(tv);

                dialog.cancel();
            }
        });
        alertDialog = alertDialogBuilder.create();

        dashboard = new Intent(this, DashboardActivity.class);



        goToDashboard = (LinearLayout) findViewById(R.id.goToDashboardButton);
        goToDashboard.setOnClickListener(new View.OnClickListener() {

           @Override

            public void onClick(View v) {

                finish();

            }
        });

        LinearLayout addNewNoteButton = (LinearLayout) findViewById(R.id.addNewNoteButton);
        addNewNoteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.result, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.note){
            alertDialog.show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SAVE_Comment_ID, commentID);
    }

    private String formatTime(int time){
        String result = " ";
        int hour = (int) time/60;
        int min = time%60;

        if (hour == 0){
            result = result + min + " minutes";
        }else if (hour == 1){
            result = result + hour + " hour "+min+" minutes";
        }else{
            result = result + hour + " hours "+min+" minutes";
        }
        return result;
    }

}
