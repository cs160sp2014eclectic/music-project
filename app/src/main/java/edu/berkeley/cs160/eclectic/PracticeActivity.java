package edu.berkeley.cs160.eclectic;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class PracticeActivity extends Activity {

    public static final String SONG_ID = "com.cs160.eclectic.ID";
    public static final String Practice_ID = "com.cs160.eclectic.Practice_ID";
    public static final String SAVE_PracticeID_ID = "com.cs160.eclectic.SAVEPRACTICEID";

    private static int practiceID = 1;
    private static int DEFAULT_TEMPO = 120;

    private static int[] sourceTempos = { 40, 42, 44, 46, 48, 50, 52, 54, 56, 58, 60, 63, 66, 69, 72,
            76, 80, 84, 88, 92, 96, 100, 104, 108, 112, 116, 120, 126, 132, 138, 144, 152, 160, 168, 176,
            184, 192, 200, 208 };

    int songID;
    private NumberPicker tempo;
    private int actualTempo = DEFAULT_TEMPO;
    Button startPractice;
    Intent listening;
    Intent editing;
    TextView BPM;
    AlphaAnimation animation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practice);

        if (savedInstanceState != null) {
            practiceID = savedInstanceState.getInt(SAVE_PracticeID_ID);
        }

        // Initialize the NumberPicker.
        tempo = (NumberPicker) findViewById(R.id.tempoPicker);

        // Tempolist taken from this page:
        // https://www.sibeliusblog.com/tips/updated-plug-in-round-metronome-marks/

        String[] tempoList = new String[sourceTempos.length];
        for (int i = 0; i < tempoList.length; i++)
            tempoList[i] = Integer.toString(sourceTempos[i]);

        tempo.setDisplayedValues(tempoList);
        tempo.setMaxValue(tempoList.length - 1);
        tempo.setMinValue(0);
        tempo.setWrapSelectorWheel(true);

        tempo.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int oldVal, int newVal) {
                actualTempo = sourceTempos[newVal];
                animation.setDuration(60 * 1000 / actualTempo);
            }
        });

        // keyboard hack
        EditText input = findInput(tempo);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);

        // End Tempolist


        // BPM blinker
        animation = new AlphaAnimation(1.0f, 0.5f);

        Bundle extras = getIntent().getExtras();
        songID = extras.getInt(SONG_ID);

        Song s = JournalActivity.db.getSong(songID);
        TextView sTitle = (TextView) findViewById(R.id.titleText);
        sTitle.setText(Utils.capitalizeFirstChar(s.title));
        TextView sAuthor = (TextView) findViewById(R.id.authorText);
        sAuthor.setText(Utils.capitalizeFirstChar(s.author));
        TextView avgTempoDescriptionText = (TextView) findViewById(R.id.tempoDescription);
        TextView avgTempoText = (TextView) findViewById(R.id.tempoText);
        int avgTempo = getAverageBpm(s);
        if (avgTempo == 0) {
            avgTempoDescriptionText.setText("");
            avgTempoText.setText("Let's get practicing!");
            tempo.setValue(getClosestIndex(DEFAULT_TEMPO));
        } else {
            avgTempoText.setText(getAverageBpm(s) + " BPM");
            int avgBPM = getAverageBpm(s);
            tempo.setValue(getClosestIndex(avgBPM));
        }

        listening = new Intent(this, ListeningActivity.class);

        startPractice = (Button) findViewById(R.id.startPractice);

        startPractice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Practice p = new Practice(practiceID, songID, "", getDateTime(), actualTempo, 0);
                long practiceDbRow = JournalActivity.db.insertPractice(p);
                listening.putExtra(SONG_ID, songID);
                listening.putExtra(Practice_ID, JournalActivity.db.getPracticeIdFromRowId(practiceDbRow));
                practiceID++;
                startActivity(listening);
            }
        });

        BPM = (TextView) findViewById(R.id.BPM);
        BPM.setShadowLayer(10, 0, 0, Color.GRAY);
        animation.setDuration(60 * 1000 / actualTempo);
        animation.setStartOffset(0);
        animation.setRepeatCount(Animation.INFINITE);
        BPM.startAnimation(animation);

        editing = new Intent(this, UpdatingActivity.class);
        Button editSong = (Button) findViewById(R.id.editSong);
        editSong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editing.putExtra(SONG_ID, songID);
                startActivity(editing);
            }
        });

        // Code for hiding the scrolling keyboard
        SetupUI(findViewById(R.id.AddingActivityouterlayout));
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SAVE_PracticeID_ID, practiceID);
    }

    private int getAverageBpm(Song s) {
        ArrayList<Practice> practices = JournalActivity.db.getAllPractices(s.sid);
        int totalTempo = 0;
        if (practices.size() == 0) {
            return 0;
        }

        for (Practice p : practices) {
            totalTempo += p.tempo;
        }
        return totalTempo / practices.size();
    }

    // Returns the closest index to the passed in tempo
    private int getClosestIndex(int tempo) {
        for (int index = 0; index < sourceTempos.length; index++) {
            if (tempo <= sourceTempos[index]) {
                return index;
            }
        }
        return sourceTempos.length - 1;
    }

    // Hacky workarourd for numberpicker keyboard
    // https://stackoverflow.com/questions/18794265/restricting-android-numberpicker-to-numeric-keyboard-for-numeric-input-not-alph
    private EditText findInput(ViewGroup np) {
        int count = np.getChildCount();
        for (int i = 0; i < count; i++) {
            final View child = np.getChildAt(i);
            if (child instanceof ViewGroup) {
                findInput((ViewGroup) child);
            } else if (child instanceof EditText) {
                return (EditText) child;
            }
        }
        return null;
    }

    // Code for hiding the keyboard

    public void hideSoftKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
    }

    public void SetupUI(View view) {
        if (!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard();
                    return false;
                }
            });
        }

        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                SetupUI(innerView);
            }
        }
    }

    // End code for hiding keyboard
}