package edu.berkeley.cs160.eclectic;

import android.app.ActivityGroup;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.CalendarView;
import android.widget.TabHost;

public class DashboardActivity extends ActivityGroup {

    public static final String SONG_ID="com.cs160.eclectic.ID";
    public static final String SAVE_SONG_ID="com.cs160.eclectic.SAVESONGID";

    int songID;

    TabHost tabHost;

    CalendarView calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        if(savedInstanceState != null){
            songID = savedInstanceState.getInt(SAVE_SONG_ID);
        }else {
            Bundle extras = getIntent().getExtras();
            songID = extras.getInt(SONG_ID);
        }

        Song song = JournalActivity.db.getSong(songID);

        setTitle(Utils.capitalizeFirstChar(song.title));


        tabHost = (TabHost) findViewById(R.id.tabHost);
        tabHost.setup(getLocalActivityManager());


        TabHost.TabSpec practice = tabHost.newTabSpec("Practice");
        TabHost.TabSpec stat = tabHost.newTabSpec("Statistics");

        Intent practiveIntent = new Intent(this, PracticeActivity.class);
        practiveIntent.putExtra(SONG_ID, songID);
        practice.setIndicator("Practice");
        practice.setContent(practiveIntent);

        Intent statIntent = new Intent(this, StatActivity.class);
        statIntent.putExtra(SONG_ID, songID);
        stat.setIndicator("Statistics");
        stat.setContent(statIntent);

        tabHost.addTab(practice);
        tabHost.addTab(stat);

        // Used for closing the keyboard
        // Variant from here:
        // http://debugreport.blogspot.com/2012/09/how-to-hide-android-soft-keyboard.html
        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(tabHost.getApplicationWindowToken(), 0);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard, menu);
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SAVE_SONG_ID, songID);
    }
}
