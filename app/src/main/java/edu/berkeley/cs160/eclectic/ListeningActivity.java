package edu.berkeley.cs160.eclectic;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import be.hogent.tarsos.dsp.MicrophoneAudioDispatcher;
import be.hogent.tarsos.dsp.beatroot.BeatRootOnsetEventHandler;
import be.hogent.tarsos.dsp.onsets.ComplexOnsetDetector;
import jp.kshoji.javax.sound.midi.MidiSystem;


public class ListeningActivity extends Activity implements BeatRootOnsetEventHandler.OnBeatHandler {

    public static final String SONG_ID="com.cs160.eclectic.ID";
    public static final String Practice_ID="com.cs160.eclectic.Practice_ID";
    public static WaveformView waveformView;

    public static final int MIN_BPM = 30;
    public static final int MAX_BPM = 280;
    public static final int DRIFT_TOLERANCE = 7;

    int songID;
    int practiceID;
    long startTime;

    int targetTempo;

    ImageButton donePracticing;
    ImageButton cancel;
    Intent result;
    Intent canceling;

    MicrophoneAudioDispatcher audioDispatcher;
    ComplexOnsetDetector detector;
    BeatRootOnsetEventHandler beatHandler;
    BeatProcessor beatProcessor;
    TransitionDrawable background;
    ScoreCalculator scorer;

    int fadeDuration;
    int emptyDuration;
    int flashDuration;
    int waitDuration;

    boolean onTempo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listening);
        MidiSystem.initialize(ListeningActivity.this);



        startTime = System.currentTimeMillis()/1000;

        Bundle extras = getIntent().getExtras();
        songID = extras.getInt(SONG_ID);
        practiceID = extras.getInt(Practice_ID);

        Song song = JournalActivity.db.getSong(songID);
        Practice practice = JournalActivity.db.getPractice(songID, practiceID);

        targetTempo = practice.tempo;
        scorer = new ScoreCalculator(new Double(targetTempo));

        setTitle(song.title);

        canceling = new Intent(this, DashboardActivity.class);
        result = new Intent(this, ResultActivity.class);

        TextView Practicing = (TextView) findViewById(R.id.Practicing);
        Practicing.bringToFront();

        donePracticing = (ImageButton) findViewById(R.id.donePracticing);
        donePracticing.bringToFront();

        cancel = (ImageButton) findViewById(R.id.cancel);
        cancel.bringToFront();

        onTempo = true;


        donePracticing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                audioDispatcher.stop();
                long endTime  = System.currentTimeMillis()/1000;
                long duration = (endTime-startTime)/60;
                //TODO Calculate the score
                String score = ((int) scorer.calculateScore()) + "%";
                JournalActivity.db.updatePractice(practiceID, songID, (int) duration, score);
                result.putExtra(SONG_ID, songID);
                result.putExtra(Practice_ID, practiceID);
                startActivity(result);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                audioDispatcher.stop();
                JournalActivity.db.deletePractice(practiceID, songID);
                finish();
            }
        });

        beatHandler = new BeatRootOnsetEventHandler();

        // Create a new audio dispatcher and adding two AudioProcessors to it.
        int size = 512;
        int overlap = 256;

        audioDispatcher = new MicrophoneAudioDispatcher(44100, size, overlap);
        detector = new ComplexOnsetDetector(size);
        detector.setHandler(beatHandler);

        beatProcessor = new BeatProcessor(size);
        audioDispatcher.addAudioProcessor(detector);
        audioDispatcher.addAudioProcessor(beatProcessor);

        waveformView = (WaveformView) findViewById(R.id.waveform);
        //waveformView.setBackgroundColor(getResources().getColor(R.color.backgroundBlue));
        //waveformView.setBackgroundColor(getResources().getColor(R.color.backgroundOrange));
        waveformView.toggleNormalPaint();
        background = (TransitionDrawable) waveformView.getBackground();
        //waveformView.toggleErrorPaint();
        //waveformView.bringToFront();

        // MicrophoneAudioProcessor is a Runnable, so start it in its own Thread.
        (new Thread(audioDispatcher)).start();

        // Spawn new thread to periodcally fetch BPM readings.
        (new Thread(new Runnable() {
            @Override
            public void run() {
                while (audioDispatcher.isDispatching()) {
                    SystemClock.sleep(7000);

                    // reset handler to collect a new set of audio events
                    BeatRootOnsetEventHandler oldHandler = beatHandler;

                    beatHandler = new BeatRootOnsetEventHandler();
                    detector.setHandler(beatHandler);

                    // Process BPM
                    oldHandler.trackBeats(ListeningActivity.this);
                }
            }
        })).start();

        // Spawn new thread for metronome visuals
        (new Thread(new Runnable() {
            @Override
            public void run() {
                int beatPeriod = (int) Math.round((60.0 / (new Double(targetTempo))) * 1000);

                flashDuration = 70;
                waitDuration = 30;

                emptyDuration = beatPeriod - flashDuration - waitDuration;

                if (emptyDuration > 114 + 50) {
                    fadeDuration = emptyDuration - 50;
                } else {
                    fadeDuration = 100;
                }

                while (audioDispatcher.isDispatching()) {
                    if (!onTempo) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                waveformView.toggleErrorPaint();
                                background.startTransition(flashDuration);
                            }
                        });
                        SystemClock.sleep(flashDuration + waitDuration);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                waveformView.toggleNormalPaint();
                                background.reverseTransition(fadeDuration);
                            }
                        });
                        SystemClock.sleep(emptyDuration);
                    } else {
                        SystemClock.sleep(emptyDuration);
                    }
                }
            }
        })).start();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        audioDispatcher.stop();
        MidiSystem.terminate();
    }

    @Override
    public void onPause() {
        // Stop the audio dispatcher so that the AudioRecord object is freed.
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void handleTempoOnset(double ibi) {
        long tempo = BeatRootOnsetEventHandler.ibiToBpm(ibi);
        System.out.println("BPM detected: " + tempo);
        ArrayList<Integer> bpms = generatePossibleBPM(ibi);
        onTempo = checkInTempo(ibi, bpms);
        System.out.println("In tempo: " + onTempo);
        scorer.sendUpdate(new Double(findClosestBpm(targetTempo, bpms)));
    }

    /**
     * Generates the possible multiples of the BPM (factors of 2) within the possible tempo range.
     *
     * @param ibi
     * @return An arraylist containing all the possible BPMs as integers.
     */
    private ArrayList<Integer> generatePossibleBPM(double ibi) {
        ArrayList<Integer> bpms = new ArrayList<Integer>();
        for (double i = ibi; BeatRootOnsetEventHandler.ibiToBpm(i) >= MIN_BPM; i *= 2) {
            bpms.add((int) BeatRootOnsetEventHandler.ibiToBpm(i));
        }
        for (double i = ibi; BeatRootOnsetEventHandler.ibiToBpm(i) <= MAX_BPM; i /= 2) {
            bpms.add((int) BeatRootOnsetEventHandler.ibiToBpm(i));
        }
        return bpms;
    }

    /**
     * Checks whether the detected tempo is within the target tempo.
     *
     * @param ibi
     * @return True if the detected tempo is within the tolerance of the target tempo
     */
    private boolean checkInTempo(double ibi, ArrayList<Integer> bpms) {
        for(Integer i: bpms) {
            if (Math.abs(i - targetTempo) <= DRIFT_TOLERANCE) {
                return true;
            }
        }
        return false;
    }

    private int findClosestBpm(int target, ArrayList<Integer> bpms) {
        int minDist = 10000;
        int minBpm = target;
        for (Integer b: bpms) {
            if (Math.abs(b - minBpm) < minDist) {
                minDist = Math.abs(b - minBpm);
                minBpm = b;
            }
        }
        return minBpm;
    }
}
