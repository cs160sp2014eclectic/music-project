package edu.berkeley.cs160.eclectic;

import java.io.Serializable;

public class Practice implements Serializable{

    public int pid;
    public int sid;
    public String score;
    public String date;
    public int tempo;
    public int ptime;

    public Practice(int p, int sID, String s, String d, int t, int time){
        sid = sID;
        pid = p;
        score = s;
        date = d;
        tempo = t;
        ptime = time;
    }

    public void setPracticeId(int p) {
        this.pid = p;
    }
}