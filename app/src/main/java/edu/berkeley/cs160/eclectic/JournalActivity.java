package edu.berkeley.cs160.eclectic;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;

import java.util.ArrayList;


public class JournalActivity extends Activity {

    public static final String SONG_ID="com.cs160.eclectic.ID";

    SwipeListView entryList;
    Intent addIntent;
    Intent statDashboard;
    public static DataBaseHandler db = null;

    ArrayList<Integer> IDArray;
    ArrayList<Song> allSongs;
    ArrayList<Item> allTitles;

    MyAdapter adapter;

    int width;
    static int deleting = -1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_journal);

        entryList = (SwipeListView) findViewById(R.id.entryList);


        addIntent = new Intent(this, AddingActivity.class);

        statDashboard = new Intent(this, DashboardActivity.class);

        if(db == null) {
            db = new DataBaseHandler(getApplicationContext(), null, 1);
        }

        updateSongs();

        updateListView();


        entryList.setSwipeListViewListener(new BaseSwipeListViewListener() {

            @Override
            public void onOpened(int position, boolean toRight) {
            }

            @Override
            public void onClosed(int position, boolean fromRight) {
            }

            @Override
            public void onListChanged() {
                if(deleting != -1){
                    entryList.dismiss(deleting);
                    entryList.closeAnimate(deleting);
                    JournalActivity.db.deleteSong(IDArray.get(deleting));
                    JournalActivity.db.deletePracticeComments(IDArray.get(deleting));
                    updateSongs();
                    deleting = -1;
                }

            }

            @Override
            public void onMove(int position, float x) {
                entryList.setOffsetRight(width-findViewById(R.id.delete).getWidth());
            }

            @Override
            public void onStartOpen(int position, int action, boolean right) {
                Log.d("swipe", String.format("onStartOpen %d - action %d", position, action));
            }

            @Override
            public void onStartClose(int position, boolean right) {
                Log.d("swipe", String.format("onStartClose %d", position));
            }

            @Override
            public void onClickFrontView(int position) {
                Log.d("swipe", String.format("onClickFrontView %d", position));
                statDashboard.putExtra(SONG_ID, IDArray.get(position));
                startActivity(statDashboard);
            }

            @Override
            public void onClickBackView(int position) {
                Log.d("swipe", String.format("onClickBackView %d", position));
                entryList.closeAnimate(position);

            }

            @Override
            public void onDismiss(int[] reverseSortedPositions) {
                for (int position : reverseSortedPositions) {
                    allTitles.remove(position);
                }
                adapter.notifyDataSetChanged();
            }

        });
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.journal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.add){
            add();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*
    Adding a new entry to the practice entries
     */
    private void add(){
        startActivity(addIntent);
    }

    private ArrayList<Item> songToTitle(ArrayList<Song> songs){
        ArrayList<Item> result = new ArrayList<Item>();
        if(!songs.isEmpty()){
            for(Song s: songs){
                ArrayList<Practice> allPractices = JournalActivity.db.getAllPractices(s.sid);
                String description;
                if (allPractices.isEmpty()){
                    description = "Created " + Utils.createPrettyRelativeDate(s.stime);
                }else{
                    description = "Last practiced " + Utils.createPrettyRelativeDate(allPractices
                            .get(allPractices.size() - 1).date);
                }
                Item item = new Item(Utils.capitalizeFirstChar(s.title),description, s.sid, s.author);
                result.add(item);
            }
        }
        return result;
    }



    /*
    This creates an array of all songs' id
     */
    private ArrayList<Integer> IDArrayCalculator(ArrayList<Song> songs){
        ArrayList<Integer> result = new ArrayList<Integer>();
        for(Song s: songs){
            result.add(s.sid);
        }
        return result;
    }

    @Override
    protected void onResume(){
        super.onResume();
        updateSongs();
        updateListView();
    }

    public void updateSongs(){
        allSongs = db.getAllSongs();
        IDArray = IDArrayCalculator(allSongs);
    }

    public void updateListView(){
        if (!allSongs.isEmpty()){
            RelativeLayout relative = (RelativeLayout) findViewById(R.id.relativeLayout);
            relative.setBackground(null);
        }
        allTitles = songToTitle(allSongs);
        adapter = new MyAdapter(this, allTitles);
        entryList.setAdapter(adapter);
    }

    private void populateDB(){
        Song s1 = new Song(0, "Symphony N0. 5", "Beethoven", "C", "Major", 10, "2014-03-10 10:49:38");
        Song s2 = new Song(0, "War Requiem", "Benjamin Britten", "D", "Minor", 5, "2014-04-10 10:52:38");
        Song s3 = new Song(0, "Goldberg Variations", "Bach", "E", "Major", 15, "2014-04-10 10:59:01");
        Song s4 = new Song(0, "Tosca", "Puccini", "C#", "Major", 4, "2014-04-10 11:08:38");
        Song s5 = new Song(0, "Cello Concerto", "Elgar", "C", "Major", 2, "2014-04-10 11:20:38");

        int s1id = db.getSongIdFromRowId(db.insertSong(s1));
        int s2id = db.getSongIdFromRowId(db.insertSong(s2));
        int s3id = db.getSongIdFromRowId(db.insertSong(s3));
        int s4id = db.getSongIdFromRowId(db.insertSong(s4));
        int s5id = db.getSongIdFromRowId(db.insertSong(s5));

        Practice p11 = new Practice(0, s1id, "79", "2014-04-10 10:50:38", 120, 127);db.insertPractice(p11);
        Practice p12 = new Practice(0, s1id, "81", "2014-04-11 08:50:38", 120, 300);db.insertPractice(p12);
        Practice p13 = new Practice(0, s1id, "87", "2014-04-12 09:50:38", 120, 40);db.insertPractice(p13);
        Practice p14 = new Practice(0, s1id, "84", "2014-04-14 07:50:38", 120, 500);db.insertPractice(p14);
        Practice p15 = new Practice(0, s1id, "90", "2014-04-15 11:50:38", 120, 30);db.insertPractice(p15);
        Practice p16 = new Practice(0, s1id, "94", "2014-04-16 10:30:38", 120, 70);db.insertPractice(p16);
        Practice p17 = new Practice(0, s1id, "92", "2014-04-18 10:24:38", 120, 83);db.insertPractice(p17);
        Practice p18 = new Practice(0, s1id, "89", "2014-04-19 10:12:38", 120, 93);db.insertPractice(p18);
        Practice p19 = new Practice(0, s1id, "87", "2014-04-20 07:50:38", 120, 186);db.insertPractice(p19);
        Practice p110 = new Practice(0, s1id, "94", "2014-04-21 11:50:38", 120, 25);db.insertPractice(p110);
        Practice p111 = new Practice(0, s1id, "95", "2014-04-21 12:50:38", 120, 90);db.insertPractice(p111);
        Practice p112 = new Practice(0, s1id, "95", "2014-04-22 10:50:38", 120, 70);db.insertPractice(p112);
        Practice p113 = new Practice(0, s1id, "87", "2014-03-20 07:50:38", 120, 186);db.insertPractice(p113);
        Practice p114 = new Practice(0, s1id, "94", "2014-03-26 11:50:38", 120, 25);db.insertPractice(p114);
        Practice p115 = new Practice(0, s1id, "95", "2014-03-27 12:50:38", 120, 90);db.insertPractice(p115);
        Practice p116 = new Practice(0, s1id, "95", "2014-03-29 10:50:38", 120, 70);db.insertPractice(p116);



        Practice p31 = new Practice(0, s3id, "72", "2014-04-27 10:50:38", 120, 127);db.insertPractice(p31);
        Practice p32 = new Practice(0, s3id, "78", "2014-04-28 08:50:38", 120, 300);db.insertPractice(p32);
        Practice p33 = new Practice(0, s3id, "83", "2014-04-29 09:50:38", 120, 40);db.insertPractice(p33);
        Practice p34 = new Practice(0, s3id, "87", "2014-05-01 07:50:38", 120, 500);db.insertPractice(p34);

    }

}
