package edu.berkeley.cs160.eclectic;

import java.util.ArrayList;

/**
 * A basic tempo score calculator determining the percent deviation.
 *
 * @author tj
 *
 */
public class ScoreCalculator {

    private double goalTempo;
    private ArrayList<Double> dataPoints;

    /**
     * Calculates a score based on how accurate the user has matched the goal
     * tempo. The performs this calculation based on a series of BPM data points
     * when the calculateScore() method is called.
     *
     * @param _goalTempo
     *          The goal tempo
     */
    public ScoreCalculator(double _goalTempo) {
        goalTempo = _goalTempo;

        dataPoints = new ArrayList<Double>();
    }

    /**
     * Adds a data point to our calculator. Each data point should be an individual
     * BPM. The more BPM data points are included, the more accurate the calculated
     * result.
     *
     * @param _datapoint
     *          A single BPM datapoint
     */
    public void sendUpdate(double _datapoint) {
        dataPoints.add(_datapoint);
    }

    /**
     * Calculates the percentage accuracy based on how well the user has been
     * playing. It uses a simple percent deviation algorithm. The algorithm will
     * also ignore outlier tempos of +- 20%.
     *
     * @return A percentage score
     */
    public double calculateScore() {

        double sum = 0.0d;
        double summedPoints = 0;
        for (Double d : this.dataPoints) {
            double point = d.doubleValue();
            double deviation = Math.abs(this.goalTempo - point);
            if (deviation < 1.2d * this.goalTempo) {
                // Remove outlier tempos
                sum += point;
                summedPoints++;
            }
        }

        if (summedPoints <= 0) {
            return 0.00d;
        }
        double mean = sum / summedPoints;
        double deviation = Math.abs(mean - this.goalTempo);

        double percentDeviation =  1 - (deviation / this.goalTempo);
        return percentDeviation * 100.0d;
    }

}