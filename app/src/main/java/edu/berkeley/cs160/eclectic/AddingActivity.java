package edu.berkeley.cs160.eclectic;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class AddingActivity extends Activity {

    public static final String SONG_ID="com.cs160.eclectic.ID";
    public static final String SAVE_SONG_ID="com.cs160.eclectic.SAVESONGID";

    private static int songID = 1;

    EditText title;
    EditText author;
    Spinner key;
    Spinner mode;
    NumberPicker practiceGoal;
    Button addToJournalButton;

    String _title="";
    String _author="";
    String _key;
    String _mode;
    int _goal;

    final Context context = this;
    Intent practiceDashboard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adding);

        if(savedInstanceState != null){
            songID = savedInstanceState.getInt(SAVE_SONG_ID);
        }

        title = (EditText) findViewById(R.id.titleEditText);
        author = (EditText) findViewById(R.id.authorEditText);
        key = (Spinner) findViewById(R.id.keySpinner);
        mode = (Spinner) findViewById(R.id.modeSpinner);

        practiceGoal = (NumberPicker) findViewById(R.id.hourPicker);
        practiceGoal.setMaxValue(40);
        practiceGoal.setMinValue(1);

        addToJournalButton = (Button) findViewById(R.id.addToJournalButton);

        practiceDashboard = new Intent(this, DashboardActivity.class);


        ArrayAdapter<CharSequence> keyAdapter = ArrayAdapter.createFromResource(this,
                R.array.key_array, android.R.layout.simple_spinner_item);
        keyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        key.setAdapter(keyAdapter);
        key.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                _key = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                _key = (String) parent.getItemAtPosition(key.getFirstVisiblePosition());
            }
        });

        ArrayAdapter<CharSequence> modeAdapter = ArrayAdapter.createFromResource(this,
                R.array.mode_array, android.R.layout.simple_spinner_item);
        modeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mode.setAdapter(modeAdapter);
        mode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                _mode = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                _mode = (String) parent.getItemAtPosition(mode.getFirstVisiblePosition());
            }
        });

        addToJournalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _title = title.getText().toString();
                _author = author.getText().toString();
                _goal = practiceGoal.getValue();

                if (_title.equals("")) {
                    AlertDialog.Builder errorAlertBuilder = new AlertDialog.Builder(context);
                    errorAlertBuilder.setMessage("Title cannot be empty.")
                              .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                  @Override
                                  public void onClick(DialogInterface dialogInterface, int i) {
                                      dialogInterface.cancel();
                                      findViewById(R.id.titleEditText).requestFocus();

                                  }
                              });
                    AlertDialog errorAlert = errorAlertBuilder.create();
                    errorAlert.show();
                } else {

                    Song song = new Song(songID, _title, _author, _key, _mode, _goal, getDateTime());
                    long rid = JournalActivity.db.insertSong(song);

                    practiceDashboard.putExtra(SONG_ID, JournalActivity.db.getSongIdFromRowId(rid));

                    //not necessary anymore
                    songID++;

                    startActivity(practiceDashboard);
                }
            }
        });
        SetupUI(findViewById(R.id.addingParent));
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    public void hideSoftKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager)  this.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
    }

    public void SetupUI(View view){
        if(!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard();
                    return false;
                }
            });
        }

        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                SetupUI(innerView);
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SAVE_SONG_ID, songID);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.adding, menu);
        return true;
    }
}
