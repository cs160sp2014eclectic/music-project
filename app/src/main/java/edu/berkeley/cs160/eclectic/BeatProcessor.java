package edu.berkeley.cs160.eclectic;

import be.hogent.tarsos.dsp.AudioEvent;
import be.hogent.tarsos.dsp.AudioProcessor;
/**
 * Created by anthony on 5/6/14.
 */
public class BeatProcessor implements AudioProcessor {

    public static int[] soundBufferAmplitude;
    private int targetBpm;

    public BeatProcessor(int bufferSize) {
        soundBufferAmplitude = new int[bufferSize];
    }

    public boolean process(AudioEvent audioEvent) {
        byte[] buffer = audioEvent.getByteBuffer();
        // do stuff with the buffer
        if (buffer.length >= soundBufferAmplitude.length) {
            for (int i = 0; i < soundBufferAmplitude.length; i++) {
                soundBufferAmplitude[i] = (int) ((buffer[2 * i] | 0x0000) + ((buffer[2 * i + 1]
                        | 0x0000) << 8));
            }
        }
        return true;
    }

    public void processingFinished() {
        // do nothing
    }
}