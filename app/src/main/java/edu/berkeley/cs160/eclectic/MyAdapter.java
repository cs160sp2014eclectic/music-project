/*
 * Copyright (C) 2013 47 Degrees, LLC
 *  http://47deg.com
 *  hello@47deg.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.berkeley.cs160.eclectic;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;


import java.util.List;

public class MyAdapter extends BaseAdapter {

    private List<Item> data;
    private Context context;
    public ViewHolder holder;

    public MyAdapter(Context context, List<Item> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Item getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Item item = getItem(position);
        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(R.layout.row, parent, false);
            holder = new ViewHolder();
            holder.title = (TextView) convertView.findViewById(R.id.frontTitle)
                                                     .findViewById(R.id.title);
            holder.author = (TextView) convertView.findViewById(R.id.frontTitle)
                    .findViewById(R.id.author);
            holder.by = (TextView) convertView.findViewById(R.id.frontTitle)
                    .findViewById(R.id.by);
            holder.date = (TextView) convertView.findViewById(R.id.date);
            holder.deleteButton = (Button) convertView.findViewById(R.id.delete);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ((SwipeListView)parent).recycle(convertView, position);

        holder.title.setText(item.getTitle());
        holder.date.setText(item.getDate());
        holder.author.setText(item.getAuthor());
        if(item.getAuthor().equals("")){
            holder.by.setText("");
        }

        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JournalActivity.deleting = position;

                notifyDataSetChanged();
            }
        });

        return convertView;
    }

    static class ViewHolder {
        TextView title;
        TextView by;
        TextView author;
        TextView date;
        Button deleteButton;
    }

}
