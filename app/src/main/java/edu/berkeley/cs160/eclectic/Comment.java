package edu.berkeley.cs160.eclectic;

public class Comment {

    public int cid;
    public String comment;
    public int pid;
    public String ctime;

    public Comment(int c, String com, int p, String time){
        cid = c;
        comment = com;
        pid = p;
        ctime = time;
    }
}