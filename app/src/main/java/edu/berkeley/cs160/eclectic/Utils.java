package edu.berkeley.cs160.eclectic;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by anthony on 4/22/14.
 */
public class Utils {
    public static String capitalizeFirstChar(String s) {
        if (s.length() == 0) {
            return s;
        } else if (s.length() == 1) {
            return s.toUpperCase();
        }
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }

    public static String createReadableDatetime(String date) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
        try {
            Date d = df.parse(date);
            return (new SimpleDateFormat("MMM dd, yyyy")).format(d);
        } catch (ParseException e) {
            return "a billion years ago";
        }
    }

    public static String createPrettyRelativeDate(String date) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
        try {
            Date d = df.parse(date);
            long delta = ((new Date()).getTime() - d.getTime()) / 1000;
            if (delta < 60) {
                return "a few seconds ago";
            }

            long deltaMinutes = delta / 60;
            if (deltaMinutes == 1)
                return deltaMinutes + " minute ago";
            if (deltaMinutes < 60)
                return deltaMinutes + " minutes ago";

            long deltaHours = deltaMinutes / 60;
            if (deltaHours == 1)
                return deltaHours + " hour ago";
            if (deltaHours < 24)
                return deltaHours + " hours ago";

            long deltaDays = deltaHours / 24;
            if (deltaDays == 1)
                return deltaDays + " day ago";
            if (deltaDays < 7)
                return deltaDays + " days ago";

            long deltaWeeks = deltaDays / 7;
            if (deltaWeeks == 1)
                return deltaWeeks + " week ago";
            if (deltaWeeks < 4)
                return deltaWeeks + " weeks ago";
            return (new SimpleDateFormat("MMM dd, yyyy")).format(d);
        } catch (ParseException e) {
            return "never";
        }
    }
}
