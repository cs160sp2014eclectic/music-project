package edu.berkeley.cs160.eclectic;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;


public class PracticeEntryActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practice_entry);

        Intent i = getIntent();
        Practice data = (Practice) i.getSerializableExtra("entry");

        TextView title = (TextView) findViewById(R.id.pEntryTitle);
        String song_title = JournalActivity.db.getSong(data.sid).title;
        setTitle(song_title);
        title.setText("Practice Entry for " + Utils.capitalizeFirstChar(song_title ));

        // TextView for the date and duration of the practice session
        TextView dateAndDuration = (TextView) findViewById(R.id.practiceDate);
        dateAndDuration.setText("Practiced on " + Utils.createReadableDatetime(data.date) + " for" +
                " " + data.ptime + " " +
                "minutes.");
        // TextView for the score of practice session
        TextView score = (TextView) findViewById(R.id.practiceScore);
        score.setText("Score: " + data.score);

        // TextView for tempo of practice
        TextView tempoText = (TextView) findViewById(R.id.tempoText);
        tempoText.setText("Tempo set to " + data.tempo + " BPM");

        ArrayList<Comment> allNotes = JournalActivity.db.getAllComments(data.pid);
        LinearLayout notesLayout = (LinearLayout) findViewById(R.id.pEntryNotesLayout);
        for (Comment c : allNotes) {
            TextView tv = new TextView(this);
            tv.setText("- " + c.comment);
            tv.setPadding(0, 10, 0, 10);
            notesLayout.addView(tv);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.practice_entry, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

}
