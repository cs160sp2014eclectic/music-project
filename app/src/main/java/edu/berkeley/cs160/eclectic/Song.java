package edu.berkeley.cs160.eclectic;

public class Song {

    public int sid;
    public String title;
    public String author;
    public String key;
    public String mode;
    public int goal;
    public String stime;

    public Song(int s, String t, String a, String k, String m, int g, String time){
        sid = s;
        title = t;
        author = a;
        key = k;
        mode = m;
        goal = g;
        stime = time;
    }
}